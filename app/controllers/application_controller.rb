class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  require 'net/ping'

  def index
  end
  
  def tap
    query = Hash.new
    # query[:default] = `ping -c 1 "#{params[:host]}"`
    query[:host] = params[:host]
    query[:alive] = Net::Ping::External.new(params[:host]).ping? if params[:host]
    render json: query 
  end
end
